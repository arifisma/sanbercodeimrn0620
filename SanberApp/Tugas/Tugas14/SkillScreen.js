import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import data from './skillData.json'

const SkillItem = ({ skillName, categoryName, percentageProgress }) => {
    
    return (
      <View style={styles.skillItems}>
        <View>
            <Image source={require('./components/Images/logo-react.png')}
                    style={{resizeMode: 'contain'}}></Image>
        </View>
        <View>
            <Text style={{fontSize: 18, fontWeight: 'bold'}}>{skillName}</Text>
            <Text style={{fontSize: 14, fontWeight: 'bold', color:'#3EC6FF'}}>{categoryName}</Text>
            <Text style={{fontSize: 40, fontWeight: 'bold', color:'#FFFFFF',
                        alignSelf: 'flex-end'}}>{percentageProgress}</Text>
        </View>
        <View>
            <Image source={require('./components/Images/chevron-forward.png')}
                    style={{resizeMode: 'contain'}}></Image>
        </View>
        
        
      </View>
    );
  }



export default class App extends React.Component {
    render () {
        return (
            <View style={styles.container}>
                <View>
                    <Image source={require('./components/Images/logo.png')} style={styles.logo}></Image>
                </View>
                
                <View style={styles.account}>
                    <Image source={require('../Tugas13/assets/account.png')} style={styles.accLogo} />
                        <View style={{marginLeft:5}}>
                            <Text style={{fontSize:12}}>Hai,</Text>
                            <Text style={{fontSize:14, color:'#003366'}}>Arif Isma,</Text>
                        </View>                    
                </View>
                
                <View style={{borderBottomWidth: 3, borderBottomColor: '#3EC6FF'}}>
                    <Text style={{fontSize:28, color:'#003366'}}>SKILL</Text>
                </View>

                <View style={{flexDirection:'row', justifyContent: 'space-around'}}>
                    <TouchableOpacity style={styles.filterText}>
                        <Text style={{color:'#003366', margin:5, fontSize:12}}>Library / Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.filterText}>
                        <Text style={{color:'#003366', margin:5, fontSize:12}}>Bahasa Pemrograman</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.filterText}>
                        <Text style={{color:'#003366', margin:5, fontSize:12}}>Teknologi</Text>
                    </TouchableOpacity>
                </View>

                <View>
                <FlatList
                    data = {data.items}
                    renderItem={({ item }) => <SkillItem
                                                skillName={item.skillName}
                                                categoryName={item.categoryName}
                                                percentageProgress={item.percentageProgress}/>}
                    keyExtractor={item => item.id}
                />
                </View>

                


            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 10,

    },
    logo: {
        width: 187,
        height: 51,
        alignSelf: 'flex-end'
    },
    account: {
        flexDirection: 'row',
        // borderColor: 'blue',
        // borderWidth:1,
        alignItems: 'center'

    },
    accLogo: {
        width: 32,
        height: 32,
        resizeMode: 'contain',
    },
    filterText: {
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        margin: 5,
    },
    skillItems: {
        flexDirection: 'row',
        backgroundColor: '#B4E9FF',
        margin: 10,
        width: 343,
        height: 129,
        alignItems: 'center',
        justifyContent: 'space-between'
    }


})