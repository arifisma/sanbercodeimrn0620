import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class App extends React.Component {
    render () {
       return (
        <View style={styles.container}>
            <View style={{alignItems:'center'}}>
                <Text style={{fontSize:30}}>Tentang Saya</Text>
                {/* <Icon name="account-circle" size={200} /> */}
                <TouchableOpacity>
                        <Image source={require('./assets/account.png')} style={styles.logo} />
                        </TouchableOpacity>
                <Text style={{fontSize:20, fontWeight:'bold', color:'#003366'}}>Arif Isma</Text>
                <Text style={{fontSize:14, fontWeight:'bold', color:'#3EC6FF'}}>Data Scientist</Text>
            </View>
            
            
            <View style={styles.portofolio}>
                <Text style={{margin:5}}>Portofolio</Text>
                <View style={{borderBottomColor:'#003366',
                    borderBottomWidth:1,
                    marginHorizontal:5}}></View>
                <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                    <View style={styles.portofolio_details}>
                        <TouchableOpacity>
                            <Image source={require('./assets/logo-gitlab.png')} style={styles.logo} />
                        </TouchableOpacity>
                        <Text>@arifisma</Text>
                    </View>

                    <View style={styles.portofolio_details}>
                        <TouchableOpacity>
                            <Image source={require('./assets/logo-github.png')} style={styles.logo} />
                        </TouchableOpacity>
                        <Text>@arifisma</Text>
                    </View>
                </View>
            </View>

            <View style={styles.contact}>
                <Text style={{margin:5}}>Hubungi saya</Text>
                <View style={{borderBottomColor:'#003366',
                    borderBottomWidth:1,
                    marginHorizontal:5}}></View>
                <View style={styles.contact}>
                    <View style={styles.contact_details}>
                    <TouchableOpacity>
                        <Image source={require('./assets/logo-facebook.png')} style={styles.logo} />
                    </TouchableOpacity>
                    <View style={{marginLeft:15}}></View>
                        <Text>arifisma</Text>
                    </View>
                    
                    <View style={styles.contact_details}>
                    <TouchableOpacity>
                        <Image source={require('./assets/logo-instagram.png')} style={styles.logo} />
                    </TouchableOpacity>
                    <View style={{marginLeft:15}}></View>
                        <Text>@arifisma</Text>
                    </View>

                    <View style={styles.contact_details}>
                    <TouchableOpacity>
                        <Image source={require('./assets/logo-twitter.png')} style={styles.logo} />
                    </TouchableOpacity>
                    <View style={{marginLeft:15}}>
                        <Text>@arifisma</Text>
                    </View>
                    
                    </View>
                </View>
            </View>
        </View>
       )
       }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        //justifyContent: 'space-evenly'        
    },
    portofolio: {
        backgroundColor: '#EFEFEF',
        borderRadius: 16
    },
    portofolio_details: {
        alignItems: 'center',
        margin: 5
        
    },
    contact: {
        marginTop: 10,
        backgroundColor: '#EFEFEF',
        borderRadius: 16
    },
    contact_details: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
})

