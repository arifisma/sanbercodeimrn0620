import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
  Button
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends React.Component {
    render () {
       return (
        <View style={styles.container}>
            <View style={styles.top_container}>
                <Image source={require('./assets/logo.png')} style={styles.logo}></Image>
            </View>

            <View style={styles.top_container}>
                <Text style={{fontSize:20, alignSelf:'center'}}>Login</Text>
            </View>

            <View style={styles.middle_container}>
                <Text style={{fontSize:12}}>Username / Email</Text>
                <TextInput style={styles.input} defaultValue="username/email"/>
                <Text style={{fontSize:12}}>Password</Text>
                <TextInput style={styles.input} defaultValue="password"/>
            </View>

            <View style={styles.bottom_container}>
                <TouchableOpacity style={styles.login_button}>
                    <Text style={{fontSize:20, alignSelf:'center', color:'#FFFFFF'}}>Masuk</Text>
                </TouchableOpacity>
                <Text style={{fontSize:20, color:'#3EC6FF'}}>Atau</Text>
                <TouchableOpacity style={styles.daftar_button}>
                    <Text style={{fontSize:20, alignSelf:'center', color:'#FFFFFF'}}>Daftar?</Text>
                </TouchableOpacity>
            </View>
        </View>
      
         )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        justifyContent: 'space-evenly'        
    },
    top_container: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    middle_container: {
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
    bottom_container: {
        alignItems: 'center'
    },
    logo: {
        width: 300,
        height: 82
    },
    input: {
        height: 40,
        width: 250,
        borderColor: 'gray',
        borderWidth: 1
      },
      login_button: {
        width:140,
        height:40,
        elevation: 10,
        backgroundColor: "#3EC6FF",
        borderRadius: 16,
        paddingVertical: 5,
        // paddingHorizontal: 12
      },
      daftar_button: {
        width:140,
        height:40,
        elevation: 10,
        backgroundColor: "#003366",
        borderRadius: 16,
        paddingVertical: 5,
        // paddingHorizontal: 12
      }
})