// Soal No. 1 Looping menggunakan while
console.log("LOOPING PERTAMA")
var x = 2;
while (x < 21) {
    console.log(x, "- I love coding")
    x += 2
}

console.log("LOOPING KEDUA")
var y = 20;
while (y > 0) {
    console.log(y, "- I will become a mobile developer")
    y -= 2
}



//Soal No. 2 Looping menggunakan for
//A. Jika angka ganjil maka tampilkan Santai
//B. Jika angka genap maka tampilkan Berkualitas
//C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
var z = 1;
for(z; z < 21; z++) {
    if (z % 3 == 0 && z % 2 == 1) {
        console.log(z, "- I Love Coding")
    } else if (z % 2 == 1) {
        console.log(z, "- Santai")
    } else {
        console.log(z, "- Berkualitas")
    }
}



// Soal No. 3 Membuat Persegi Panjang
// persegi dengan dimensi 8×4 dengan tanda pagar (#) dengan perulangan
console.log("SOAL NOMOR 3")
var panjang = 8;
var tinggi = 4;
var c = ""

while (tinggi > 0) {
    // tambahkan '#' pada variabel c sampai sejumlah 8 karakter, cetak, dan pindah ke baris selanjutnya
    for (panjang; panjang > 0; panjang--) {
        c += "#"
    }
    // cetak per baris 
    console.log(c)
    tinggi-=1 // sisa nilai variabel tinggi
}



// Soal No. 4 Membuat Tangga
// menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi 7 dan alas 7
console.log("SOAL NOMOR 4")
var tinggi = 7;
var alas = 7;
var baris = 0;
var d = "";

while (tinggi > 0) {
    // tambahkan '#' ke variabel d kemudiang langsung cetak dan pindah ke baris selanjutnya
    for (baris; baris < tinggi; baris++) {
        d += "#"
        console.log(d) // cetak setelah menambahkan satu '#'
        }
    tinggi-=1 // sisa tinggi
}


// Soal No. 5 Membuat Papan Catur
// sebuah papan catur dengan ukuran 8 x 8 . Papan berwarna hitam memakai tanda pagar (#) sedangkan papan putih menggunakan spasi
// JAWABAN
// Step (1) buat pola ganjil genap, (2) cetak pola
console.log("SOAL NOMOR 5")
var sisi = 8; // panjang dan lebar papan catur
var baris = 1; // baris (total 8 baris)
var kolom = 1; // kolom (total 8 kolom)
pola_ganjil = "" // pola pada baris ganjil
pola_genap = "" // pola pada baris genap

// membuat pola baris ganjil dan genap
for (kolom; kolom < sisi+1; kolom++) { // looping kolong 1 - 8
    if (kolom % 2 == 1) { // kolom ganjil
        pola_ganjil += " " // kolom ganjil baris ganjil berwarna putih
        pola_genap += "#" // kolom ganjil baris genap berwarna hitam
    } else { // kolom genap
        pola_ganjil += "#"
        pola_genap += " "
    }
    }

// looping 8 baris dan cetak pola
while (baris < sisi+1) {
    if (baris % 2 == 1) { // baris ganjil
        console.log(pola_ganjil)
    } else { // baris genap
        console.log(pola_genap)
    }
    baris += 1
}