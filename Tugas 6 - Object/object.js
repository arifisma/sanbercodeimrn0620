// SOAL NOMOR 1

function arrayToObject(input_array){
    var peopleArray = [] //utk menampung kumpulan personObj
    var now = new Date()
    var thisYear = now.getFullYear()

    //loop utk konversi data
    for (person=0; person < input_array.length; person++) {
        // Jika tahun tidak terdefinisi atau tahunnya lebih besar dibandingkan dengan tahun sekarang maka kembalikan nilai : “Invalid birth year”
        umur = thisYear - input_array[person][3]

        // convert data di dalam array menjadi object
        if (input_array[person][3] === undefined || input_array[person][3] > thisYear) {
            umur = "Invalid Birth Year"
        }

        var personObj = {
            firstName: input_array[person][0],
            lastName: input_array[person][1],
            gender: input_array[person][2],
            age: umur
        }
        peopleArray.push(personObj)
        // cetak disini supaya tidak ada perulangan lagi sewaktu mencetak ke konsol
        //console.log(`${person+1}. ${personObj.firstname} ${personObj.lastname} :`, personObj)
    }
    
    //loop utk cetak data ke konsol
    var indeks = 0;
    while (indeks < peopleArray.length) {
        console.log(
            `${indeks+1}. ${peopleArray[indeks].firstName} ${peopleArray[indeks].lastName}:`, peopleArray[indeks]
        )
        indeks+=1
    }
    
    return peopleArray
}


var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject([])

// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""



// SOAL NOMOR 2
function shoppingTime(memberId, money) {    //parameter String dan Number
    // you can only write your code here!
    if (memberId === undefined || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        // do nothing, run the next lines of codes
    }

    sisa_uang = money;
    var output = {
        memberId: memberId,
        money: money,
        listPurchased: [],
        changeMoney: sisa_uang
    }

    // daftar produk dengan susunan acak
    var productsObj = [
        {item: "Sepatu Stacattu",
        price: 1500000},
        {item: "Baju Zoro",
        price: 500000},
        {item: "Baju H&N",
        price: 250000},
        {item: "Sweater Uniklooh",
        price: 175000},
        {item: "Casing Handphone",
        price: 50000}
    ]
    
    // sort dafar produk berdasarkan harga Descending
    productsObj.sort(function (value1, value2) { return value2.price - value1.price } ) ;
    
    listPurchased = []
    for (item=0; item < productsObj.length && sisa_uang >= 50000; item++) {
        //console.log(productsObj[0].price)
        //console.log(productsObj[item])
        if (sisa_uang < productsObj[item].price) {
            productsObj.splice(item,1)
        } else {
            //do nothing
            listPurchased.push(productsObj[item].item)
            sisa_uang-=productsObj[item].price
        }
    }
    
    // update variable output
    output.listPurchased = listPurchased
    output.changeMoney = sisa_uang
    return output
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja




//Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrPenumpang2 = []

    // loop utk memasukkan data objek penumpang ke daftar arrPenumpang2
    for (index = 0; index < arrPenumpang.length; index++) {
        start = arrPenumpang[index][1]
        finish = arrPenumpang[index][2]
        ongkos = (rute.indexOf(finish) - rute.indexOf(start)) * 2000
        
        var personObj = {
            penumpang: arrPenumpang[index][0],
            naikDari: start,
            tujuan: finish,
            bayar: ongkos
        }
        arrPenumpang2.push(personObj);
    }
    return arrPenumpang2
  }
   
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
//   [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//     { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
console.log(naikAngkot([])); //[]
//console.log(naikAngkot([["Dimitri", "B", "F"]]))