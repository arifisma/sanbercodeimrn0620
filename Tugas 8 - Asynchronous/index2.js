var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function bacaBuku(waktu, buku) {
    readBooksPromise(waktu, buku[0])
        .then(sisaWaktu => readBooksPromise(sisaWaktu, buku[1]))
        .then(sisaWaktu => readBooksPromise(sisaWaktu, buku[2]))
        .catch(function (error) {
            //do nothing;
        })
}


bacaBuku(10000, books) 