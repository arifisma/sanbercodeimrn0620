// Soal No. 1 (Range)
/*
Buatlah sebuah function dengan nama range() yang menerima dua parameter berupa number. Function mengembalikan sebuah array yang berisi angka-angka mulai dari angka parameter pertama hingga angka pada parameter kedua. Jika parameter pertama lebih besar dibandingkan parameter kedua maka angka-angka tersusun secara menurun (descending).
struktur fungsinya seperti berikut range(startNum, finishNum) {}
Jika parameter pertama dan kedua tidak diisi maka function akan menghasilkan nilai -1
*/
function range(startNum, finishNum) {
    var result = [] // array untuk menampung nilai antara startNum dan finishNum
    if (arguments.length < 2) { // apabila startNum / finishNum tidak diisi
        return -1
    } else if (startNum < finishNum ) {
        while (startNum <= finishNum) {
            result.push(startNum)
            startNum += 1
         }
    } else {
        while (startNum >= finishNum) {
            result.push(startNum)
            startNum -= 1
        }
    }
    return result
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1



//Soal No. 2 (Range with Step)
function rangeWithStep(startNum, finishNum, step) {
    var result = []
    if (startNum === undefined || finishNum === undefined) {
        //return -1
    } else if (startNum < finishNum ) {
        for (startNum; startNum <= finishNum; startNum += step) {
            result.push(startNum)
         }
    } else {
        for (startNum; startNum >= finishNum; startNum -= step) {
            result.push(startNum)
        }
    }
    return result
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 



//Soal No. 3 (Sum of Range)
/*Buatlah sebuah function dengan nama sum() yang menerima tiga parameter yaitu angka awal deret, angka akhir deret, dan beda jarak (step). Function akan mengembalikan nilai jumlah (sum) dari deret angka. contohnya sum(1,10,1) akan menghasilkan nilai 55.
ATURAN: Jika parameter ke-3 tidak diisi maka stepnya adalah 1.
*/
function sum(startNum, finishNum, step) {
    var total = 0; // default nilai
    if (startNum == undefined && finishNum === undefined) {
        // do nothing
    } else if (finishNum === undefined) {
        total = startNum
    } else {
        if (typeof(step)==='undefined') step = 1; // add default values for variable step
        var newList = rangeWithStep(startNum,  finishNum, step)
        //console.log(newList)
    
        for (var i=0; i < newList.length; i++) {
            total += newList[i]
        }
    }
    
    return total
}

// Code di sini
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 



//Soal No. 4 (Array Multidimensi)
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(arrayInput) {
    for (rows=0; rows < arrayInput.length; rows++) {
        console.log(`Nomor ID: ${arrayInput[rows][0]}` )
        console.log(`Nama Lengkap: ${arrayInput[rows][1]}` )
        console.log(`TTL: ${arrayInput[rows][2]} ${arrayInput[rows][3]}` )
        console.log(`Hobi: ${arrayInput[rows][4]}` )
        console.log("")
    }
}

dataHandling(input)


//Soal No. 5 (Balik Kata)
//Buatlah sebuah function balikKata() yang menerima sebuah parameter berupa string dan mengembalikan kebalikan dari string tersebut
function balikKata(teks) {
    return teks.split("").reverse().join("")
}


console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I



//Soal No. 6 (Metode Array)
function dataHandling2(input){
    input.splice(4,1,"Pria", "SMA Internasional Metro")
    input.splice(1,2,"Roman Alamsyah ElSharawy", "Provinsi "+input[2])
    return input
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log(dataHandling2(input));
 
//Berdasarkan elemen yang berisikan tanggal/bulan/tahun (elemen ke-4),
//ambil angka bulan dan console.log nama bulan sesuai dengan angka tersebut.
TTL = input[3].split("/")
bulan = TTL[1]
switch (bulan) {
    case "01": {bulan = "Januari"; break;}
    case "02": {bulan = "Februari"}
    case "03": {bulan = "Maret"; break;}
    case "04": {bulan = "April"; break;}
    case "05": {bulan = "Mei"; break;}
    case "06": {bulan = "Juni"; break;}
    case "07": {bulan = "Juli"; break;}
    case "08": {bulan = "Agustus"; break;}
    case "09": {bulan = "September"; break;}
    case "10": {bulan = "Oktober"; break;}
    case "11": {bulan = "November"; break;}
    default: {bulan = "Desember" ; break;}
}
console.log(bulan)

//
TTL1 = input[3].split("/")
TTL1.sort(function (value1, value2) { return value2 - value1 } ) ; 
console.log(TTL1)
//
TTL2 = TTL.join("-")
console.log(TTL2)


// Nama maksimum 15 karakter
nama = String(input[1]) //memastikan input objek dalam bentuk String
nama = nama.slice(0,14) //maksimum 15 karakter
console.log(nama)

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 