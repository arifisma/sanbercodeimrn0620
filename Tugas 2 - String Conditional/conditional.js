//Soal If-Else

//  WARNING: variable peran case sensitive
var nama = "John";
var peran = "penyihir"; // pilihan peran: Penyihir, Guard, dan Werewolf

var openingMessage = "Selamat datang di dunia Werewolf, " + nama

if ( nama == "") { // output jika nama tidak diisi
    console.log("Nama harus diisi!")
} else if(peran == "") { // output jika nama diisi tapi peran tidak diisi 
    console.log("Halo " + nama +", Pilih peranmu untuk memulai game!")
} else { // apabila nama dan peran telah diisi
    if(peran == "Penyihir") {
        console.log(openingMessage)
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
    } else if(peran == "Guard") {
        console.log(openingMessage)
        console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if(peran == "Werewolf") {
        console.log(openingMessage)
        console.log("Halo Werewolf " + nama + ", kamu akan memakan mangsa setiap malam!")
    } else {
        console.log("Peran " + peran + " tidak ditemukan. Peran tersedia: Penyihir, Guard, Werewolf (case sensitive)")
    }
    }





// Soal Switch Case
var tanggal = 31; 
var bulan = 12; 
var tahun = 2200;


if (tanggal > 0 && tanggal < 32 && tahun > 1899 && tahun < 2201) { // periksa validitas tanggal dan tahun
    switch(bulan) {
        case 1:   { console.log(tanggal, 'Januari', tahun); break; }
        case 2:   { console.log(tanggal, 'Februari', tahun); break; }
        case 3:   { console.log(tanggal, 'Maret', tahun); break; }
        case 4:   { console.log(tanggal, 'April', tahun); break; }
        case 5:   { console.log(tanggal, 'Mei', tahun); break; }
        case 6:   { console.log(tanggal, 'Juni', tahun); break; }
        case 7:   { console.log(tanggal, 'Juli', tahun); break; }
        case 8:   { console.log(tanggal, 'Agustus', tahun); break; }
        case 9:   { console.log(tanggal, 'September', tahun); break; }
        case 10:   { console.log(tanggal, 'Oktober', tahun); break; }
        case 11:   { console.log(tanggal, 'November', tahun); break; }
        case 12:   { console.log(tanggal, 'Desember', tahun); break; }
        default:  { console.log("Bulan", bulan, "tidak ada lho.."); }}
} else {
    console.log("Tanggal/tahun tidak valid. (Tgl antara 1-31, tahun antara 1900-2200)")
}
